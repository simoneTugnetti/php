<!DOCTYPE html>

<!--Simone Tugnetti-->

<html lang="it">
<head>
    <title>Conto</title>
</head>
<body>
    <?php
        $anti=$_GET['anti'];
        $primi=$_GET['primi'];
        $secondi=$_GET['secondi'];
        $dolci=$_GET['dolci'];
        $bibite=$_GET['bibite'];
        $domicilio=$_GET['dom'];
        $prezzo=$anti+$primi+$secondi+$dolci+$bibite+$domicilio;

        echo "<h1>Scontrino</h1><br><br>";
        echo "<table border='1'>";
        echo "<tr><td>Pietanze</td><td>Prezzo</td></tr>";
        echo "<tr><td>Antipasto</td>";
        switch($anti){
            case 1.20:
                echo "<td>Focaccia alla genovese - €1.20</td>";
            break;
            case 2.50:
                echo "<td>Arancini - €2.50</td>";
            break;
            case 3.00:
                echo "<td>Insalata di polipo - €3.00</td>";
            break;
        }
        echo "</tr>";
        echo "<tr><td>Primo</td>";
        switch($primi){
            case 4.30:
                echo "<td>Spaghetti alle vongole - €4.30</td>";
            break;
            case 5.00:
                echo "<td>Risotto ai carciofi - €5.00</td>";
            break;
            case 3.80:
                echo "<td>Crema di asparigi - €3.80</td>";
            break;
        }
        echo "</tr>";
        echo "<tr><td>Secondo</td>";
        switch($secondi){
            case 3.70:
                echo "<td>Involtini di vitello - €3.70</td>";
            break;
            case 6.20:
                echo "<td>Sarde fritte - €6.20</td>";
            break;
            case 4.10:
                echo "<td>Roast beef - €4.10</td>";
            break;
        }
        echo "</tr>";
        echo "<tr><td>Dolce</td>";
        switch($dolci){
            case 4.20:
                echo "<td>Tiramisù - €4.20</td>";
            break;
            case 2.50:
                echo "<td>Sorbetto - €2.50</td>";
            break;
            case 3.10:
                echo "<td>Profitterol - €3.10</td>";
            break;
        }
        echo "</tr>";
        echo "<tr><td>Bibita</td>";
        switch($bibite){
            case 1.00:
                echo "<td>Acqua naturale - €1.00</td>";
            break;
            case 1.50:
                echo "<td>Birra chiara - €1.50</td>";
            break;
            case 2.00:
                echo "<td>Coca cola - €2.00</td>";
            break;
        }
        echo "</tr>";
        echo "<tr><td>Servizio a domicilio</td>";
        switch($domicilio){
            case 2:
                echo "<td>SI - €2</td>";
            break;
            case 0:
                echo "<td>NO</td>";
            break;
        }
        echo "</tr>";
        
        echo "<tr><td>Prezzo totale</td><td>€ $prezzo</td></tr>";
        echo "</table>"
    ?>
</body>
</html>