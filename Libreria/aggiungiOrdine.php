<!doctype html>

<!--Simone Tugnetti-->

<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Verifica Aggiunta Ordine</title>
		<link rel="icon" href="img/icon.png">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	
	<body>
		<?php
			
			$conn = new mysqli("localhost","root","","libreria");
		
			function randomCodice($length = 10) {
    			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    			$charactersLength = strlen($characters);
    			$randomString = '';
    			for ($i = 0; $i < $length; $i++) {
        			$randomString .= $characters[rand(0, $charactersLength - 1)];
    			}
    			return $randomString;
			}	
			
			$codiceOrdine = randomCodice();
		
			$query = " INSERT INTO acquista VALUES ('$codiceOrdine','$_POST[IDClienteS]','$_POST[ISBNLibroS]','$_POST[dataAcquistoL]')";
       		$result = $conn->query($query);
       		echo "<h1 class='text-center'>L'ordine &egrave; stato aggiunto al database!</h1>";
			echo "<h2 class='text-center'>Verrai reindirizzato nella homepage!</h2>";
			$conn->close();
		?>
		<script type="text/javascript">
			setTimeout(function(){
            	window.location.href = "index.php";
         	}, 5000);
		</script>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>