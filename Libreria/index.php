<!doctype html>

<!--Simone Tugnetti-->

<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="Description" content="Libreria Bella senza nome, perchè non esistono parole per descriverla">
		<title>Libreria Bella</title>
		<link rel="icon" href="img/icon.png">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="css/personale.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	</head>
	<body>
		<!-----------Header--------------->
		<?php
			$connection = @ new mysqli("localhost","root","","libreria");
			if ($connection->connect_error){
				die ("Errore di connessione con il DBMS.");
			}
			$connection->close();
		?>
		<?php
			session_start();
			$_SESSION['Cliente']=$_SESSION['CodiceCliente'];
		?>
		<header id="intro">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top fixed-top" title="menu">
    			<div class="container">
      				<a class="navbar-brand" href="index.html" target="_blank" title="logo">Libreria Bella</a>
      				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        				<span class="navbar-toggler-icon"></span>
      				</button>
      				<div class="collapse navbar-collapse" id="navbarResponsive">
        				<ul class="navbar-nav ml-auto" id="ulNav">
          					<li class="nav-item active" title="Home">
            					<a class="nav-link" href="#intro">Intro</a>
          					</li>
          					<li class="nav-item" title="Catalogo">
            					<a class="nav-link" href="#catalogo">Catalogo</a>
          					</li>
          					<li class="nav-item" title="Ordini">
            					<a class="nav-link" href="#ordini">Ordini</a>
          					</li>
          					<li class="nav-item mr-5" title="Privacy">
            					<a class="nav-link" href="#footer">Privacy</a>
          					</li>
							
							<?php
								if($_SESSION["Cliente"]==""){
									echo "<li class='nav-item mr-5' title='Utente'>
											<button class='btn btn-outline-success my-2 my-sm-0' type='button' 	data-toggle='modal' data-target='#accedi'>Accedi</button> 
											<button class='btn btn-outline-success my-2 my-sm-0' type='button' data-toggle='modal' data-target='#registrati'>Registrati</button>
										</li>";
								}else{
									$conn = new mysqli("localhost","root","","libreria");
									
									$query = "select nomeCognome from cliente where IDCliente='$_SESSION[Cliente]'";
				
									$result = $conn->query($query);
									if($result->num_rows!=0){
										while($row = $result->fetch_array()){
											echo "<li class='nav-item dropdown ml-5' title='Utente'>
        											<a class='nav-link dropdown-toggle' href='#' id='infoUtente' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Benvenuto, $row[nomeCognome]</a>
        												<div class='dropdown-menu' aria-labelledby='visualUtente'>
          													<a class='dropdown-item' data-toggle='modal' data-target='#$_SESSION[Cliente]'>Informazioni utente</a>
        												</div>
     												</li>
													<li class='nav-item' title='Esci'>
														<button class='btn btn-outline-success my-2 my-sm-0' type='button' data-toggle='modal' data-target='#esci'>Esci</button>
													</li>";
										}
									}
								}
							?>
        				</ul>
      				</div>
    			</div>
  			</nav>
			<div class="container-fluid intro padding-0">
				<div class="opacita flex-column d-flex justify-content-center align-items-center">
					<h1 title="Libreria Bella">Libreria Bella</h1>
					<h2 class="white">Perchè non servono parole se non quelle scritte nel viso di chi legge</h2>
				</div>
			</div>
		</header>
		<!------------Catalogo--------------->
		<section id="catalogo" class="container-fluid d-flex align-items-center flex-column mt-5">
			<h2 class="mb-5">Catalogo Libri</h2>
			<div class="container d-flex flex-row justify-content-between mb-5">
				<?php
					$conn = new mysqli("localhost","root","","libreria");
				
					$query = "select copertina,titolo,prezzo,ISBN,nomeAutore,nomeEditore from libro";
				
					$result = $conn->query($query);
					
					if($result->num_rows!=0){
						while($row = $result->fetch_array()){
							echo "<div class='card col-lg-3'>";
							echo "<img src='$row[copertina]' class='card-img-top mt-2' alt='Copertina libro' id='copLibro'>";
							echo "<div class='card-body'>";
							echo "<h5 class='card-title'>$row[titolo]</h5>";
							echo "<p class='card-text'>Prezzo: €$row[prezzo]</p>";
							echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#ISBN-$row[ISBN]'>Dettagli Libro</button>";
							echo "</div>";
							echo "</div>";
						}
					}else{
						echo "<div class='card col-lg-3 mx-auto'>";
						echo "<img src='https://cdn-images-1.medium.com/max/1200/1*zPiik9vlW_G7GU9bTjxhJQ.jpeg' class='card-img-top mt-2' alt='Copertina libro' id='copLibro'>";
						echo "<div class='card-body'>";
						echo "<h5 class='card-title'>Non vi è alcun libro disponibile</h5>";
						echo "</div>";
						echo "</div>";
					}
					$conn->close();
				?>
			</div>
		</section>
		<!--------------Ordini--------------->
		<section id="ordini" class="container-fluid d-flex align-items-center flex-column mt-5">
			<h2 class="mb-5">Lista Ordini</h2>
			<div class="container mb-5 mt-5">
				<h3 class="text-center">Inserimento Ordine</h3>
				<form method="POST" action="aggiungiOrdine.php" class="mb-3">
  					<div class="form-group">
    					<label for="listaClienti">Scegli L'IDCliente</label>
    					<select class="form-control" name="IDClienteS" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select IDCliente from cliente";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[IDCliente]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
  					<div class="form-group">
    					<label for="listaLibri">Scegli L'ISBN del libro</label>
    					<select class="form-control" name="ISBNLibroS" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select ISBN from libro";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[ISBN]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
					<div class="form-group">
    					<label for="dataAcquistoL">Data di Acquisto</label>
    					<input type="date" class="form-control" name="dataAcquistoL" placeholder="Inserisci la data di acquisto" required>
  					</div>
  					<button type="submit" class="btn btn-primary">Aggiungi Ordine</button>
				</form>
				
				<h3 class="text-center">Modifica Ordine</h3>
				<form method="POST" action="modificaOrdine.php" class="mb-3">
  					<div class="form-group">
    					<label for="listaOrdini">Scegli il codice dell'ordine</label>
    					<select class="form-control" name="listaOrdiniS" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select codiceOrdine from acquista";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[codiceOrdine]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
  					<div class="form-group">
    					<label for="listaClienti">Scegli il nuovo IDCliente</label>
    					<select class="form-control" name="listaClientiS" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select IDCliente from cliente";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[IDCliente]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
  					<div class="form-group">
    					<label for="listaLibri">Scegli il nuovo ISBN</label>
    					<select class="form-control" name="listaLibriS" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select ISBN from libro";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[ISBN]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
					<div class="form-group">
    					<label for="dataAcquistoS">Data di Acquisto</label>
    					<input type="date" class="form-control" name="dataAcquistoS" placeholder="Inserisci la data di acquisto" required>
  					</div>
  					<button type="submit" class="btn btn-primary">Modifica Ordine</button>
				</form>
				
				<h3 class="text-center">Elimina Ordine</h3>
				<form method="GET" action="eliminaOrdine.php" class="mb-3">
  					<div class="form-group">
    					<label for="listaOrdini">Scegli il codice dell'ordine</label>
    					<select class="form-control" name="listaOrdiniS2" required>
							<?php
								$conn = new mysqli("localhost","root","","libreria");
				
								$query = "select codiceOrdine from acquista";
				
								$result = $conn->query($query);
								
								if($result->num_rows!=0){
									while($row = $result->fetch_array()){
										echo "<option>$row[codiceOrdine]</option>";
									}
								}
								$conn->close();
							?>
   					 	</select>
  					</div>
					<button type="submit" class="btn btn-primary">Elimina Ordine</button>
				</form>
				
				<table class="table mt-5">
  					<thead>
    					<tr>
							<th scope="col">Codice Ordinazione</th>
      						<th scope="col">ID Cliente</th>
      						<th scope="col">ISBN Libro</th>
      						<th scope="col">Data dell'Ordine</th>
    					</tr>
  					</thead>
  					<tbody>
	  					<?php
	  						$conn = new mysqli("localhost","root","","libreria");
				
							$query = "select * from acquista";
				
							$result = $conn->query($query);
								
							if($result->num_rows!=0){
								while($row = $result->fetch_array()){
									echo "<tr>";
									echo "<th scope='row'>$row[codiceOrdine]</th>";
									echo "<td>$row[IDCliente]</td>";
									echo "<td>$row[ISBN]</td>";
									echo "<td>$row[dataAcquisto]</td>";
									echo "</tr>";
								}
							}
							$conn->close();
	  					?>
  					</tbody>
				</table>
			</div>
		</section>
		<!-------------Footer-------------->
		<footer id="footer" class="container-fluid bg-dark d-flex justify-content-between align-items-center">
			<h4 title="Privacy policy" class="white">© 2019 Libreria Bella. All rights reserved. Design by Simone Tugnetti</h4>
				<div>
					<i class="fab fa-facebook fb-color" title="Facebook"></i>
					<i class="fas fa-envelope mail-color ml-2" title="Mail"></i>
				</div>
		</footer>
		
		
		
		<!--------------Modal-------------->
		<?php
			$conn = new mysqli("localhost","root","","libreria");
				
			$query = "select * from libro";
				
			$result = $conn->query($query);
			
			if($result->num_rows!=0){
				while($row = $result->fetch_array()){
					$autore = str_replace(' ', '-', $row['nomeAutore']);
					$editore = str_replace(' ', '-', $row['nomeEditore']);
					echo "<div class='modal fade' tabindex='-1' role='dialog' id='ISBN-$row[ISBN]'>";
					echo "<div class='modal-dialog' role='document'>";
					echo "<div class='modal-content'>";
					echo "<div class='modal-header'>";
					echo "<h5 class='modal-title'>$row[titolo]</h5>";
					echo "</div>";
					echo "<div class='modal-body'>";
					echo "<p>ISBN: $row[ISBN]</p>";
					echo "<p>Descrizione: $row[descrizione]</p>";
					echo "<p>Numero di Copie: $row[nCopie]</p>";
					echo "<p>Data di pubblicazione: $row[dataPubblicazione]</p>";
					echo "<p>Autore: $row[nomeAutore]</p>";
					echo "<p>Editore: $row[nomeEditore]</p>";
					echo "</div>";
					echo "<div class='modal-footer'>";
					echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#$autore'>Dettagli Autore</button>";
					echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#$editore'>Dettagli Editore</button>";
					echo "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Chiudi scheda</button>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}
			}
		
			$query = "select * from autore";
				
			$result = $conn->query($query);
			
			if($result->num_rows!=0){
				while($row = $result->fetch_array()){
					$autore = str_replace(' ', '-', $row['nomeCognome']);
					echo "<div class='modal fade' tabindex='-1' role='dialog' id='$autore'>";
					echo "<div class='modal-dialog' role='document'>";
					echo "<div class='modal-content'>";
					echo "<div class='modal-header'>";
					echo "<h5 class='modal-title'>$row[nomeCognome]</h5>";
					echo "</div>";
					echo "<div class='modal-body'>";
					echo "<p>Luogo di nascita: $row[luogoNascita]</p>";
					echo "<p>Data di nascita: $row[dataNascita]</p>";
					if(is_null($row['dataMorte'])){
						echo "<p>Questo autore &egrave ancora in vita</p>";
					}else{
						echo "<p>Deceduto in data: $row[dataMorte]</p>";
					}
					echo "</div>";
					echo "<div class='modal-footer'>";
					echo "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Chiudi scheda</button>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}
			}
		
			$query = "select * from cliente where IDCliente='$_SESSION[Cliente]'";
				
			$result = $conn->query($query);
			
			if($result->num_rows!=0){
				while($row = $result->fetch_array()){
					echo "<div class='modal fade' tabindex='-1' role='dialog' id='$row[IDCliente]'>";
					echo "<div class='modal-dialog' role='document'>";
					echo "<div class='modal-content'>";
					echo "<div class='modal-header'>";
					echo "<h5 class='modal-title'>$row[nomeCognome]</h5>";
					echo "</div>";
					echo "<div class='modal-body'>";
					echo "<p>ID Cliente: $row[IDCliente]</p>";
					echo "<p>Nome utente: $row[nomeCognome]</p>";
					echo "<p>Indirizzo d'abitazione: $row[indirizzo]</p>";
					echo "<p>Data di nascita: $row[dataNascita]</p>";
					if($row['telefono']==0){
						echo "<p>Telefono: non fornito</p>";
					}else{
						echo "<p>Telefono: $row[telefono]</p>";
					}
					echo "<p>Indirizzo e-mail: $row[mail]</p>";
					echo "<p>Password: $row[password]</p>";
					echo "</div>";
					echo "<div class='modal-footer'>";
					echo "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Chiudi scheda</button>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}
			}
		
			$query = "select * from editore";
				
			$result = $conn->query($query);
			
			if($result->num_rows!=0){
				while($row = $result->fetch_array()){
					$editore = str_replace(' ', '-', $row['nomeEditore']);
					echo "<div class='modal fade' tabindex='-1' role='dialog' id='$editore'>";
					echo "<div class='modal-dialog' role='document'>";
					echo "<div class='modal-content'>";
					echo "<div class='modal-header'>";
					echo "<h5 class='modal-title'>$row[nomeEditore]</h5>";
					echo "</div>";
					echo "<div class='modal-body'>";
					echo "<p>Sede principale: $row[sedePrincipale]</p>";
					if(is_null($row['gruppo'])){
						echo "<p>Questo editore non &egrave affiliato a nessun gruppo editoriale</p>";
					}else{
						echo "<p>Gruppo: $row[gruppo]</p>";
					}
					if(is_null($row['sitoWeb'])){
						echo "<p>Questo editore non possiede un sito web</p>";
					}else{
						echo "<p>Sito web: <a href='http://$row[sitoWeb]' target='_blank' class='black'>$row[sitoWeb]</a></p>";
					}
					echo "</div>";
					echo "<div class='modal-footer'>";
					echo "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Chiudi scheda</button>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";
				}
			}
			$conn->close();
		?>
		
		<div class="modal fade" tabindex="-1" role="dialog" id="accedi">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h5 class="modal-title">Effettua il login</h5>
      				</div>
					<form method="post" action="accesso.php">	
      					<div class="modal-body">
   							<div class="form-group">
    							<label for="emailAccesso">Indirizzo Email</label>
    							<input type="email" class="form-control" name="emailAccesso" placeholder="Inserisci l'indirizzo e-mail" required>
  							</div>
  							<div class="form-group">
    							<label for="passwordAccesso">Password</label>
    							<input type="password" class="form-control" name="passwordAccesso" placeholder="Inserisci la password" required>
  							</div>
      					</div>
      					<div class="modal-footer">
        					<button type="submit" class="btn btn-primary">Accedi</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
      					</div>
					</form>
    			</div>
  			</div>
		</div>
		
		<div class="modal fade" tabindex="-1" role="dialog" id="registrati">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h5 class="modal-title">Effettua la Registrazione</h5>
      				</div>
					<form method="post" action="registrazione.php">	
      					<div class="modal-body">
							<div class="form-group">
    							<label for="nomeCognome">Nome e Cognome</label>
    							<input type="text" class="form-control" name="nomeCognome" placeholder="Inserisci il tuo nome e cognome" required>
  							</div>
							<div class="form-group">
    							<label for="indirizzo">Residenza</label>
    							<input type="text" class="form-control" name="indirizzo" placeholder="Inserisci l'indirizzo d'abitazione" required>
  							</div>
   							<div class="form-group">
    							<label for="dataNascitaR">Data di Nascita</label>
    							<input type="date" class="form-control" name="dataNascitaR" placeholder="Inserisci la data di nascita" required>
  							</div>
							<div class="form-group">
    							<label for="telefono">Telefono</label>
    							<input type="tel" class="form-control" name="telefono" placeholder="Inserisci il numero di telefono">
  							</div>
							<div class="form-group">
    							<label for="mailR">Indirizzo E-mail</label>
    							<input type="text" class="form-control" name="mailR" placeholder="Inserisci il tuo indirizzo email" required>
  							</div>
  							<div class="form-group">
    							<label for="passwordR">Password</label>
    							<input type="password" class="form-control" name="passwordR" placeholder="Inserisci la password" required>
  							</div>
      					</div>
      					<div class="modal-footer">
        					<button type="submit" class="btn btn-primary">Registrati</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
      					</div>
					</form>
    			</div>
  			</div>
		</div>
		
		<div class="modal fade" tabindex="-1" role="dialog" id="esci">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
        				<h5 class="modal-title">Uscita</h5>
      				</div>
					<form method="post" action="esci.php">	
      					<div class="modal-body">
							<p>Si desidera veramente uscire?</p>
						</div>
      					<div class="modal-footer">
        					<button type="submit" class="btn btn-primary">Esci</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
      					</div>
					</form>
    			</div>
  			</div>
		</div>
		
		
		<!--------JQuery Bootsrap--------->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>
