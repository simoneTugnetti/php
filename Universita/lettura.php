<!DOCTYPE html>

<!--Simone Tugnetti-->

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Università Lettura</title>
</head>
<body>
    <h1>Query di lettura</h1>
    <?php

    $connect = mysqli_connect("localhost","root","","universita");

    $query = "select * from corsi order by NomeCorso";

    $rows = $connect -> query($query);

    if($rows -> num_rows!=0){
        echo "<table border>";
        echo "<tr>";
        echo "<th>Codice corso</th>";
        echo "<th>Nome corso</th>";
        echo "<th>Codice Docente</th>";
        echo "</tr>";
        while($row = $rows -> fetch_array()){
            echo "<tr>";
            echo "<td>".$row["codCorso"]."</td>";
            echo "<td>".$row["NomeCorso"]."</td>";
            echo "<td>".$row["codDocente"]."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }else{
        echo "Non vi sono dati all'interno della tabella";
    }
    $rows -> clear();
    $connect -> close();
    ?>
</body>
</html>