<!DOCTYPE html>

<!--Simone Tugnetti-->

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Università Eliminazione</title>
</head>
<body>
    <h1>Query di eliminazione</h1>
    <?php

    $connect = mysqli_connect("localhost","root","","universita");

    $query = "delete from docente where codDocente=00005";

    $ins = $connect -> query($query);

    echo "<h2>Il docente &egrave stato eliminato!</h2>";

    $query2 = "select * from docente";

    $rows = $connect -> query($query2);

    if($rows -> num_rows!=0){
        echo "<table border>";
        echo "<tr>";
        echo "<th>Codice docente</th>";
        echo "<th>Nome docente</th>";
        echo "<th>Nome Dipartimento</th>";
        echo "</tr>";
        while($row = $rows -> fetch_array()){
            echo "<tr>";
            echo "<td>".$row["codDocente"]."</td>";
            echo "<td>".$row["NomeD"]."</td>";
            echo "<td>".$row["Dipartimento"]."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }else{
        echo "Non vi sono dati all'interno della tabella";
    }
    $connect -> close();
    ?>
</body>
</html>