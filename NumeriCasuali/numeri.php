<!DOCTYPE html>

<!--Simone Tugnetti-->

<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Numeri Casuali</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css">
	<link href="css/personale.css" rel="stylesheet" type="text/css" media="screen">
</head>
<body>
    <div class="container-fluid background alt-100">
        <h1>Numeri generati</h1>
        <?php
        echo "<h2>",rand(1,90),"</h2>";
        echo "<h2>",rand(1,90),"</h2>";
        echo "<h2>",rand(1,90),"</h2>";
        ?>
    </div>
</body>
</html>